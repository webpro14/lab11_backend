import { IsNotEmpty, IsPositive, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(8, 16)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
