import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRespository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto): Promise<Product> {
    const product: Product = new Product();
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    return this.productsRespository.save(product);
  }

  findAll(): Promise<Product[]> {
    return this.productsRespository.find();
  }

  findOne(id: number): Promise<Product> {
    return this.productsRespository.findOneBy({ id: id });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRespository.findOneBy({ id: id });
    const updateProduct = { ...product, ...updateProductDto };
    return this.productsRespository.save(updateProduct);
  }

  async remove(id: number) {
    const product = await this.productsRespository.findOneBy({ id: id });
    return this.productsRespository.remove(product);
  }
}
